<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePCzasTable extends Migration
{
    public function up()
    {
        Schema::create('p_czas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('miesiac');
            $table->integer('kwartal');
            $table->year('rok');
        });
    }

    public function down()
    {
        Schema::dropIfExists('p_czas');
    }
}
