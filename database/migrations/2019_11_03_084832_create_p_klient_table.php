<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePKlientTable extends Migration
{
    public function up()
    {
        Schema::create('p_klient', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('imie')->nullable();
            $table->string('nazwisko')->nullable();
            $table->integer('wiek')->nullable();
            $table->string('plec')->nullable();
            $table->string('zamoznosc')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('p_klient');
    }
}
