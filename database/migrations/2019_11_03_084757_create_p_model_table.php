<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePModelTable extends Migration
{
    public function up()
    {
        Schema::create('p_model', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nazwa');
            $table->string('naped');
            $table->string('nadwozie');
            $table->string('silnik_moc');
            $table->string('rodz_paliwa');
            $table->string('silnik_poj');
            $table->string('skrzynia_biegow');
            $table->bigInteger('marka_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('p_model');
    }
}
