<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePSprzedazTable extends Migration
{
    public function up()
    {
        Schema::create('p_sprzedaz', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cena');
            $table->string('zysk');
            $table->unsignedBigInteger('id_klient');
            $table->unsignedBigInteger('id_sztuka');
            $table->unsignedBigInteger('id_czas');
        });
    }

    public function down()
    {
        Schema::dropIfExists('p_sprzedaz');
    }
}
