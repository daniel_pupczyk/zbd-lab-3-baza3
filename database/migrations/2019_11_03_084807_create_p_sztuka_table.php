<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePSztukaTable extends Migration
{
    public function up()
    {
        Schema::create('p_sztuka', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kolor');
            $table->year('rok_produckji');
            $table->string('przebieg');
            $table->string('kraj_produkcji');
            $table->boolean('czy_uszkodzony');
            $table->bigInteger('model_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('p_sztuka');
    }
}
