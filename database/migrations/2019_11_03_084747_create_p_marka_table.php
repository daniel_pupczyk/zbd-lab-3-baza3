<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePMarkaTable extends Migration
{
    public function up()
    {
        Schema::create('p_marka', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nazwa');
        });
    }

    public function down()
    {
        Schema::dropIfExists('p_marka');
    }
}
