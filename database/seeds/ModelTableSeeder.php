<?php

use App\PModel;
use Illuminate\Database\Seeder;

class ModelTableSeeder extends Seeder
{
    public function run()
    {
        for ($i = 0; $i < 500; $i++) {
            $model = factory(PModel::class)->make();

            $isExist = PModel::where('nazwa', $model->nazwa)->first();

            if (!$isExist) {
                $model->save();
            }
        }
    }
}
