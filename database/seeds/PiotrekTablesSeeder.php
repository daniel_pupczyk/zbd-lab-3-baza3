<?php

use App\PCzas;
use App\PKlient;
use App\PSztuka;
use Illuminate\Database\Seeder;

class PiotrekTablesSeeder extends Seeder
{
    public function run()
    {
        $ilosc = 50;

        factory(PSztuka::class, $ilosc)->create();
        factory(PKlient::class, $ilosc)->create();
        factory(PCzas::class, $ilosc)->create();

        $sprzedazCount = \App\PSprzedaz::count();

        factory(\App\PSprzedaz::class, $ilosc)
            ->make()
            ->each(function ($s, $index) use ($sprzedazCount) {
                $s['id_klient'] = $sprzedazCount + $index + 1;
                $s['id_sztuka'] = $sprzedazCount + $index + 1;
                $s['id_czas'] = $sprzedazCount + $index + 1;

                $s->save();
            });
    }
}
