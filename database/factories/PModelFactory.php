<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PMarka;
use App\PModel;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(PModel::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Fakecar($faker));
    $v = $faker->vehicleArray();

    $marka = PMarka::where('nazwa', $v['brand'])->first();

    if (!$marka) {
        $marka = PMarka::create([
            'nazwa' => $v['brand'],
        ]);
    }

    return [
        'marka_id' => $marka->id,
        'nazwa' => $v['model'],
        'naped' => $faker->randomElement($array = array('x2', 'x4')),
        'nadwozie' => $faker->vehicleType,
        'skrzynia_biegow' => $faker->vehicleGearBoxType,
        'rodz_paliwa' => $faker->vehicleFuelType,
        'silnik_moc' => $faker->numberBetween($min = 20, $max = 5000),
        'silnik_poj' => $faker->numberBetween($min = 1000, $max = 5000),
    ];
});
