<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\PSprzedaz;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(PSprzedaz::class, function (Faker $faker) {
    $cena = $faker->numberBetween($min = 500, $max = 5000000);

    return [
        'cena' => $cena,
        'zysk' => $faker->numberBetween($min = 500, $max = $cena),
        'id_klient' => 1,
        'id_sztuka' => 1,
        'id_czas' => 1,
    ];
});
