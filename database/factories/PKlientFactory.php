<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PKlient;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(PKlient::class, function (Faker $faker) {
    return [
        'imie' => $faker->firstName,
        'nazwisko' => $faker->lastName,
        'wiek' => $faker->numberBetween($min = 18, $max = 60),
        'plec' => $faker->randomElement($array = array('male', 'female', '')),
        'zamoznosc' => $faker->randomElement($array = array('mala', 'srednia', 'duza', '')),
    ];
});
