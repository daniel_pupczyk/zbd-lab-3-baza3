<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PModel;
use App\PSztuka;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(PSztuka::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Fakecar($faker));

    $modelCount = PModel::count();

    return [
        'kolor' => $faker->colorName,
        'rok_produckji' => $faker->year,
        'przebieg' => $faker->numberBetween($min = 1000, $max = 500000),
        'kraj_produkcji' => $faker->country,
        'czy_uszkodzony' => $faker->numberBetween($min = 0, $max = 1),
        'model_id' => $faker->numberBetween($min = 1, $max = $modelCount),
    ];
});
