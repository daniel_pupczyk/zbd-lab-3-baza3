<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PSztuka extends Model
{
    public $timestamps = false;

    protected $table = 'p_sztuka';
}
