<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateDB extends Command
{
    protected $signature = 'generate:db';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('start:' . date('Y-m-d H:i:s'));

        $this->call('db:seed', [
            '--class' => 'ModelTableSeeder',
        ]);

        $this->call('db:seed', [
            '--class' => 'PiotrekTablesSeeder',
        ]);

        $this->info('end:' . date('Y-m-d H:i:s'));
    }
}
