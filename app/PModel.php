<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PModel extends Model
{
    public $timestamps = false;

    protected $table = 'p_model';
}
