<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PSprzedaz extends Model
{
    public $timestamps = false;

    protected $table = 'p_sprzedaz';
}
