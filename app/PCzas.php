<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PCzas extends Model
{
    public $timestamps = false;

    protected $table = 'p_czas';
}
