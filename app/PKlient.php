<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PKlient extends Model
{
    public $timestamps = false;

    protected $table = 'p_klient';
}
