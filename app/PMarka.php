<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMarka extends Model
{
    public $timestamps = false;

    protected $table = 'p_marka';
}
